package dev.hussein.spring.rabbitmq.producer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dev.hussein.spring.rabbitmq.producer.model.Employee;
import dev.hussein.spring.rabbitmq.producer.service.MessagingService;

@RestController
@RequestMapping("/api")
public class ProducerController {

	@Autowired
	private MessagingService messageService;
	
	@GetMapping("/produceMessage")
	public void produceMessage(@RequestParam("empName") String empName,@RequestParam("empId") String empId) {
		Employee emp = new Employee();
		emp.setEmpName("Hussein");
		emp.setEmpId("1");
		
		messageService.send(emp);
		System.out.println("message sent successfully !");
	}
}
