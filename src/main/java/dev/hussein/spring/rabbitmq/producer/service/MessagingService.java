package dev.hussein.spring.rabbitmq.producer.service;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import dev.hussein.spring.rabbitmq.producer.model.Employee;

@Service
public class MessagingService {

	@Autowired
	private AmqpTemplate rabbitTemplate;
	
	@Value("${dev.rabbitmq.exchange}")
	private String exchange;
	
	@Value("${dev.rabbitmq.routingkey}")
	private String routingkey;	
	
	public void send(Employee emp) {
		rabbitTemplate.convertAndSend(exchange, routingkey, emp);
		System.out.println("Send msg = " + emp);
	    
	}
}
